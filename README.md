# CASTLE LABYRINTH #

Breakout style game. 

### Story ###

* Play through many castle rooms to free your beloved from the evil king.

### What is this repository for? ###

* Game Distribution

### How do I get set up? ###

* Download the latest RELEASE
* Unzip it
* Run "Castle Labyrinth.exe"

### Who do I talk to? ###

* Create an issue to report problems
* Create an issue to give suggestions